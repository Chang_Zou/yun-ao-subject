package com.yunao.dao;

import com.yunao.model.InputUserOne;

import java.util.ArrayList;
import java.util.List;

/**
 * 第一种数据类型结构
 * @author Sober
 */
public class InputUserOneDao {

    public static List<InputUserOne> getList() {
        List<InputUserOne> list = new ArrayList<>();
        InputUserOne a = new InputUserOne();
        a.setAge("12");
        a.setName("小刘");
        a.setIpHomePlace("(四川省,眉山市,仁寿县)");
        InputUserOne b = new InputUserOne();
        b.setAge("29");
        b.setName("小孙");
        b.setIpHomePlace("(江苏省,丰县,)");
        list.add(a);
        list.add(b);
        return list;
    }

}
