package com.yunao.model;

/**
 * 第一种来源数据
 * @author Sober
 */
public class InputUserOne {

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private String age;

    /**
     * Ip归属地
     */
    private String ipHomePlace;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIpHomePlace() {
        return ipHomePlace;
    }

    public void setIpHomePlace(String ipHomePlace) {
        this.ipHomePlace = ipHomePlace;
    }

}
