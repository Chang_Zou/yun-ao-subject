package com.yunao.model;

/**
 * 第二种数据格式
 * @author Sober
 */
public class InputUserTwo {

    /**
     * 姓名
     */
    private String consignee;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 收货地址
     */
    private String placeReceipt;

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPlaceReceipt() {
        return placeReceipt;
    }

    public void setPlaceReceipt(String placeReceipt) {
        this.placeReceipt = placeReceipt;
    }
}
