package com.yunao.reporter;


import com.yunao.dao.InputUserOneDao;
import com.yunao.model.InputUserOne;
import com.yunao.model.OutPutUser;

import java.util.List;

/**
 * 第一种来源实现
 *
 * @author Sober
 */
public class UserOneReporter extends UserReporter<InputUserOne> {

    @Override
    protected List<InputUserOne> extractData() {
        return InputUserOneDao.getList();
    }

    @Override
    protected OutPutUser analyze(InputUserOne data) {
        OutPutUser pojo = new OutPutUser();
        pojo.setAge(data.getAge());
        pojo.setUserName(data.getName());
        setLocus(
                pojo,
                analyzeIpHomePlace(data.getIpHomePlace())
        );
        return pojo;
    }

    /**
     * 解析ip归属地
     *
     * @param ipHomePlace ip归属地
     * @return String[] 地址解析数组
     */
    private String[] analyzeIpHomePlace(String ipHomePlace) {
        final String COMMA = ",";
        return ipHomePlace.substring(1, ipHomePlace.length() - 1).split(COMMA);
    }

    /**
     * 写入所在地信息
     * @param pojo 写入对象
     * @param disassemble 解析好的地址数组
     */
    private void setLocus(OutPutUser pojo, String[] disassemble) {
        for (int i = 0; i < disassemble.length; i++) {
            switch (i) {
                case 0:
                    pojo.setProvince(disassemble[i]);
                    break;
                case 1:
                    pojo.setCity(disassemble[i]);
                    break;
                case 2:
                    pojo.setCounty(disassemble[i]);
                    break;
                default:
            }
        }
    }
}
