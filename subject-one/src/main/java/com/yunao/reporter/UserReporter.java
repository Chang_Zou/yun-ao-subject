package com.yunao.reporter;

import com.yunao.model.OutPutUser;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户分析模板
 * @param <T> 具体对象类型
 * @author Sober
 */
public abstract class UserReporter<T> {

    /**
     * 生成分析的报表
     * @return 用户分析报表
     */
    public final List<OutPutUser> generateReport() {
        List<T> data = extractData();
        return data.stream().map(this::analyze).collect(Collectors.toList());
    }

    /**
     * 获取数据
     * @return 数据
     */
    protected abstract List<T> extractData();

    /**
     * 分析单个对象
     * @param data 对象
     * @return 分析完成对象
     */
    protected abstract OutPutUser analyze(T data);
}
